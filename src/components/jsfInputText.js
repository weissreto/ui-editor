export default (editor) => {

    editor.DomComponents.addType('jsf-input-text', {
        isComponent: el => el.tagName === 'H:INPUTTEXT',

        model: {
            defaults: {
                name: 'JSF Input Text',
                droppable: false,
                draggable: '[data-gjs-type=jsf-form]',
                attributes: {},
                traits: [
                    'id',
                    'value',
                    {
                        type: 'checkbox',
                        name: 'rendered'
                    },
                    'title',
                    'size',
                    'role',
                    {
                        type: 'checkbox',
                        name: 'readonly'
                    },
                    {
                        type: 'checkbox',
                        name: 'required'
                    },
                    'requiredMessage',
                    'immediate',
                    'binding',
                    'maxlenght',
                    'label',
                    'alt',
                    'accesskey',
                    'validator',
                    'validatorMessage',
                    'disabled',
                    'autocomplete',
                    'style',
                    'styleClass',
                    'tabindex',
                    'binding',
                    'accesskey',
                    'dir',
                    {
                        type: 'checkbox',
                        name: 'escape'
                    },
                    'lang'
                ]
            }
        },
        view: {
            tagName: 'input',
        }
    })
}