
export default (editor) => {

    editor.DomComponents.addType('jsf-output-label', {
        isComponent: el => el.tagName === 'H:OUTPUTLABEL',

        model: {
            defaults: {
                name: 'JSF Output Label',
                droppable: false,
                draggable: '[data-gjs-type=jsf-form]',
                attributes: {},
                traits: [
                    'id',
                    'value',
                    'for',
                    {
                        type: 'checkbox',
                        name: 'rendered'
                    },
                    'title',
                    'style',
                    'styleClass',
                    'tabindex',
                    'binding',
                    'accesskey',
                    'dir',
                    {
                        type: 'checkbox',
                        name: 'escape'
                    },
                    'lang'
                ]
            }
        },
        view: {
            tagName: 'span',
            attributes: {},
            init()
            {
                this.listenTo(this.model, 'change:attributes:value', this.render)
            },
            renderChildren() {
                let content = this.model.get('attributes')['value'];
          
                if (content) {
                  this.getChildrenContainer().innerHTML = content;
                }
            }
        }
    })
}