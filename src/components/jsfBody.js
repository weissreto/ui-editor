export default (editor) => {

    editor.DomComponents.addType('jsf-body', {
        isComponent: el => el.tagName === 'H:BODY',

        model: {
            defaults: {
                name: 'JSF Body',
                droppable: true,
                attributes: {},
                traits: []
            }
        }
    })
}
