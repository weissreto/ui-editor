import jsfBody from "./jsfBody"
import jsfForm from "./jsfForm"
import jsfOutputLabel from "./jsfOutputLabel"
import jsfInputText from "./jsfInputText"

export default (editor) => {
    jsfBody(editor);
    jsfForm(editor);
    jsfOutputLabel(editor);
    jsfInputText(editor);
}