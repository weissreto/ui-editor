export default (editor) => {

    editor.DomComponents.addType('jsf-form', {
        isComponent: el => el.tagName === 'H:FORM',

        model: {
            defaults: {
                name: 'JSF Form',
                droppable: true,
                draggable: '[data-gjs-type=jsf-body]',
                attributes: {},
                traits: []
            }
        }
    })
}